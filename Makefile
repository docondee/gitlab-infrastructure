DOCKER_COMPOSE_BIN=.docker-compose/docker-compose
DOCKER_COMPOSE_CHECKSUM_DARWIN=f9c72b3950a8846682f5cbccda27323b427dd2636611af22461c21444f019691
DOCKER_COMPOSE_CHECKSUM_LINUX=92551cd3d22b41536ce8345fe06795ad0d08cb3c17b693ecbfe41176e501bfd4
DOCKER_COMPOSE_DIR=.docker-compose
DOCKER_COMPOSE_LINK=https://github.com/docker/compose/releases/download/$(DOCKER_COMPOSE_VERSION)/docker-compose-$(shell uname -s)-$(shell uname -m)
DOCKER_COMPOSE_VERSION=v2.2.2
DOCKER_IMAGE=ubuntu-22-04
GIT_REPO_DIR=repository/openai-experimentation
GIT_REPO_URL=https://gitlab.com/docondee/openai-experimentation

# Build the Docker image
.PHONY: build-image
build-image:
	# Build the Docker image using the specified Dockerfile
	docker build . -t $(DOCKER_IMAGE) -f docker-files/ubuntu-22-04/Dockerfile;

# Check if the ubuntu-22-04 image exists, and build it if it doesn't
.PHONY: build-ubuntu-22-04-image
build-ubuntu-22-04-image:
	# Check if the ubuntu-22-04 image exists
	if docker image inspect $(DOCKER_IMAGE) &> /dev/null; then \
		echo "Docker image $(DOCKER_IMAGE) already exists, skipping build step."; \
	else \
		$(MAKE) build-image; \
	fi

# Check if the current system is a supported platform
.PHONY: check-platform
check-platform:
	# Make sure the system is either Linux or Mac
	@if [ "$(shell uname -s)" != "Linux" ] && [ "$(shell uname -s)" != "Darwin" ]; then \
		echo "Error: this makefile can only be used on Linux or Mac systems."; \
		exit 1; \
	fi

	# Make sure the architecture is x86_64
	@if [ "$(shell uname -m)" != "x86_64" ]; then \
		echo "Error: this makefile can only be used on x86_64 architectures."; \
		exit 1; \
	fi

# Clone the repository using the specified URL
.PHONY: clone-repository
clone-repository:
	$(MAKE) execute-command-in-docker COMMAND="git clone $(GIT_REPO_URL) $(GIT_REPO_DIR)"

# Remove the repository directory
.PHONY: remove-repository
remove-repository:
	$(MAKE) execute-command-in-docker COMMAND="rm -fr $(GIT_REPO_DIR)"

# Download the specified version of Docker Compose
.PHONY: download-docker-compose
download-docker-compose:
	# Download Docker Compose using the specified link
	$(MAKE) execute-command-in-docker COMMAND="curl -L $(DOCKER_COMPOSE_LINK) -o $(DOCKER_COMPOSE_BIN)"

# Run a command inside a new Docker container
.PHONY: execute-command-in-docker
execute-command-in-docker: build-ubuntu-22-04-image
	# Run the specified command inside a new Docker container
	# with the current directory mounted at /app
	docker run --rm -v "$(CURDIR):/app" $(DOCKER_IMAGE) sh -c "cd /app && $(COMMAND)"

# Install Docker Compose
.PHONY: install-docker-compose
install-docker-compose: check-platform
	# Create the .docker-compose directory
	mkdir -p $(DOCKER_COMPOSE_DIR)

# Download Docker Compose
	$(MAKE) download-docker-compose

	# Validate the checksum of the Docker Compose binary
	$(MAKE) validate-docker-compose-checksum EXPECTED_CHECKSUM="$(DOCKER_COMPOSE_CHECKSUM_$(shell uname -s | tr '[:lower:]' '[:upper:]'))"

	# Set the permissions to make the binary executable
	$(MAKE) execute-command-in-docker COMMAND="chmod +x $(DOCKER_COMPOSE_BIN)"

.PHONY: uninstall-docker-compose
uninstall-docker-compose:
	# Remove the binary and the .docker-compose directory
	$(MAKE) execute-command-in-docker COMMAND="rm -rf $(DOCKER_COMPOSE_DIR)"

# Validate the checksum of the docker-compose binary
.PHONY: validate-docker-compose-checksum
validate-docker-compose-checksum:
	# Compare the calculated checksum to the expected checksum
	$(MAKE) execute-command-in-docker COMMAND="echo '$(EXPECTED_CHECKSUM)  $(DOCKER_COMPOSE_BIN)' | sha256sum -c -;"
